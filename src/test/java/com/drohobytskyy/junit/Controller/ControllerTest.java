package com.drohobytskyy.junit.Controller;

import com.drohobytskyy.junit.Model.Game;
import com.drohobytskyy.junit.View.ConsoleView;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

@ExtendWith(MockitoExtension.class)
public class ControllerTest {

    //@InjectMocks annotation is used to create and inject the mock object
    @InjectMocks
    Controller controller = new Controller( new ConsoleView());;
    //  MathApplication mathApplication = new MathApplication();

    //@Mock annotation is used to create the mock object to be injected
    @Mock
    Game game;

    public ControllerTest() throws IOException {
    }

    @Test
    public void testGetRandomField() throws IOException {
        System.out.println(game = Mockito.mock(Game.class));
        game.start();
    }
}
