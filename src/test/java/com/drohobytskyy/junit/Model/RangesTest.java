package com.drohobytskyy.junit.Model;

import org.junit.jupiter.api.*;

import java.util.Random;

import static junit.framework.TestCase.assertTrue;

public class RangesTest {

    @BeforeAll
    public static void beforeClass() {
        System.out.println("before class: ");
    }

    @BeforeEach
    public void beforeTest() {
        System.out.println("before test :");
    }

    @AfterEach
    public void afterTest() {
        System.out.println("after test : ");
    }

    @AfterAll
    public static void afterClass() {
        System.out.println("after class is : ");
    }

    @Test
    public void inRangeTest() {
        Ranges ranges = new Ranges();
        Ranges.setSize(new Coordinates(10,10));
        assertTrue(Ranges.inRange(new Coordinates(3,3)));
    }
}
