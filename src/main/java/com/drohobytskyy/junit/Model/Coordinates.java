package com.drohobytskyy.junit.Model;

public class Coordinates {
    private int x;
    private int y;

    public Coordinates(int x, int y) {
        this.setX(x);
        this.setY(y);
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Coordinates) {
            Coordinates tempCoord = (Coordinates)obj;
            return tempCoord.getX() == this.getX() && tempCoord.getY() == this.getY();
        } else {
            return false;
        }
    }
}