package com.drohobytskyy.junit.Model;

public class Bomb {
    private Matrix bombMap;
    private int totalBombs;

    public Bomb(int totalBombs) {
        this.totalBombs = totalBombs;
        fixBombsCount();
    }

    private void fixBombsCount() {
        int maxBombs = Ranges.getSize().getX() * Ranges.getSize().getY() / 2;
        if (totalBombs > maxBombs) {
            totalBombs = maxBombs;
        }
    }

    public Matrix start() {
        bombMap = new Matrix(Box.ZERO);
        for (int i = 0; i < totalBombs; i++) {
            placeBomb();
        }
        return bombMap;
    }


    Box get(Coordinates coord) {
        return bombMap.get(coord);
    }

    private void placeBomb() {
        Coordinates coord;
        do {
            coord = Ranges.getRandomCoordinates();
        } while (bombMap.get(coord) == Box.BOMB);
        bombMap.set(coord, Box.BOMB);
        incNumbersAroundBomb(coord);
    }

    private void incNumbersAroundBomb(Coordinates coord) {
        for(Coordinates around: Ranges.getCoordinatesAround(coord)) {
            if(bombMap.get(around) != Box.BOMB) {
                bombMap.set(around, bombMap.get(around).getNextNumberBox());
            }
        }

    }

    public int getTotalBombs() {
        return totalBombs;
    }


}