package com.drohobytskyy.junit.Model;

import java.util.Iterator;

public class Matrix {
    private Box[][] matrix = new Box[Ranges.getSize().getX()][Ranges.getSize().getY()];

    public Matrix(Box defaultBox) {
        Coordinates coord;
        for(Iterator var3 = Ranges.getAllCoordinates().iterator(); var3.hasNext(); this.matrix[coord.getX()][coord.getY()] = defaultBox) {
            coord = (Coordinates)var3.next();
        }
    }

    public Box get(Coordinates coord) {
        return Ranges.inRange(coord) ? this.matrix[coord.getX()][coord.getY()] : null;
    }

    void set(Coordinates coord, Box box) {
        if (Ranges.inRange(coord)) {
            this.matrix[coord.getX()][coord.getY()] = box;
        }

    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == Box.BOMB) {
                    stringBuffer.append("x").append(" ");
                } else if (matrix[i][j] == Box.NUM1) {
                    stringBuffer.append("1").append(" ");
                } else if (matrix[i][j] == Box.NUM2) {
                    stringBuffer.append("2").append(" ");
                } else if (matrix[i][j] == Box.NUM3) {
                    stringBuffer.append("3").append(" ");
                } else if (matrix[i][j] == Box.NUM4) {
                    stringBuffer.append("4").append(" ");
                } else if (matrix[i][j] == Box.NUM5) {
                    stringBuffer.append("5").append(" ");
                } else if (matrix[i][j] == Box.NUM6) {
                    stringBuffer.append("6").append(" ");
                } else if (matrix[i][j] == Box.NUM7) {
                    stringBuffer.append("7").append(" ");
                } else if (matrix[i][j] == Box.NUM8) {
                    stringBuffer.append("8").append(" ");
                } else {
                    stringBuffer.append("  ");
                }
            }
            stringBuffer.append("\n");
        }
        return "Matrix : \n" + stringBuffer.toString();

    }
}

