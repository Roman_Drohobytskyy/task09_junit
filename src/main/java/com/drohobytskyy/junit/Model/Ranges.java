package com.drohobytskyy.junit.Model;

import java.util.ArrayList;
import java.util.Random;

public class Ranges {
    private static Coordinates size;
    private static ArrayList<Coordinates> allCoordinates;
    private static Random random = new Random();

    public Ranges() {
    }

    public static Coordinates getSize() {
        return size;
    }

    public static void setSize(Coordinates size) {
        Ranges.size = size;
        setAllCoordinates();
    }

    public static ArrayList<Coordinates> getAllCoordinates() {
        return allCoordinates;
    }

    public static void setAllCoordinates() {
        allCoordinates = new ArrayList();

        for(int x = 0; x < getSize().getX(); ++x) {
            for(int y = 0; y < getSize().getY(); ++y) {
                allCoordinates.add(new Coordinates(x, y));
            }
        }

    }

    static boolean inRange(Coordinates coord) {
        return coord.getX() >= 0 && coord.getX() < getSize().getX() && coord.getY() >= 0 && coord.getY() < getSize().getY();
    }

    static Coordinates getRandomCoordinates() {
        return new Coordinates(random.nextInt(getSize().getX()), random.nextInt(getSize().getY()));
    }

    static ArrayList<Coordinates> getCoordinatesAround(Coordinates coord) {
        ArrayList<Coordinates> list = new ArrayList();

        for(int x = coord.getX() - 1; x <= coord.getX() + 1; ++x) {
            for(int y = coord.getY() - 1; y <= coord.getY() + 1; ++y) {
                Coordinates aroundCoord;
                if (inRange(aroundCoord = new Coordinates(x, y)) && !aroundCoord.equals(coord)) {
                    list.add(aroundCoord);
                }
            }
        }

        return list;
    }
}

