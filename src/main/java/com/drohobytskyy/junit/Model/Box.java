package com.drohobytskyy.junit.Model;

public enum Box {
    ZERO,
    NUM1,
    NUM2,
    NUM3,
    NUM4,
    NUM5,
    NUM6,
    NUM7,
    NUM8,
    BOMB;

    Box getNextNumberBox() {
        return values()[this.ordinal() + 1];
    }

    int getNumber() {
        return this.ordinal();
    }
}
