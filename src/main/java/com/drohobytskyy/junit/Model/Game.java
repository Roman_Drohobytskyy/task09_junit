package com.drohobytskyy.junit.Model;

public class Game {

	private static final int MAX_PERCENTAGE = 100;
	private static final double DEFAULT_PART_OF_BOMBS = 0.25;
	Matrix bombMap;
	private Bomb bomb;

	public Game(int rows, int columns, int percentageOfBombs) {
		Ranges.setSize(new Coordinates(rows, columns));
		int countOfBombs;
		if (percentageOfBombs != 0) {
			countOfBombs = percentageOfBombs * rows * columns / MAX_PERCENTAGE;
		} else {
			countOfBombs = (int)(rows * columns * DEFAULT_PART_OF_BOMBS);
		}
		bomb = new Bomb(countOfBombs);
	}

	public Matrix start() {
		bombMap = bomb.start();
		return bombMap;
	}

	public static void main(String[] args) {
		Game game = new Game(10, 10,25);
		game.start();
	}
}
