package com.drohobytskyy.junit.View;

import com.drohobytskyy.junit.Controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class ConsoleView {
    public final static String EXIT = "exit";
    private BufferedReader input;
    public Controller controller;
    public Map<String, MenuItem> menu;
    public Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() throws IOException {
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    public void createMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", new MenuItem(" 1   - Get random minesweeper field.",
                controller::getRandomField));
        menu.put(EXIT, new MenuItem(EXIT + " - exit from app", this::exitFromMenu));
    }

    public void executeMenu(Map<String, MenuItem> menu) throws IOException {
        String keyMenu;
        do {
            logger.warn("-------------------------------------------------------------------------------------"
                    + "-------------------------------------------------");

            logger.warn("Please, select menu point.");
            outputMenu(menu);
            keyMenu = input.readLine().toLowerCase();
            try {
                menu.get(keyMenu).getLink().print();
            } catch (Exception e) {
                logger.error("Invalid input! Please try again.");
            }
        } while (true);
    }

    private void outputMenu(Map<String, MenuItem> menu) {
        for (MenuItem pair : menu.values()) {
            logger.info(pair.getDescription());
        }
    }

    public void exitFromMenu() {
        logger.warn("Have an amazing day!");
        System.exit(0);
    }

}

